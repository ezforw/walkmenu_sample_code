defmodule Walkmenu.Router do
  use Walkmenu.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :webhook do
    plug :accepts, ["json"]
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :apply_layout do
    plug Walkmenu.Plug.ApplyLayout
  end

  pipeline :rst_admin_only do
    plug Walkmenu.Plug.MustLogin
    plug Walkmenu.Plug.RstAdminOnly
  end

  pipeline :must_login do
    plug Walkmenu.Plug.MustLogin
  end

  ################################
  # routes for account
  ################################
  scope "/", Walkmenu, as: :account do
    pipe_through [:browser, :attach_login_user_info]
    post "/request_reset_password", Account.AccountController, :request_reset_password
    get "/reset_password/:token", Account.AccountController, :reset_psw_by_token_get
    put "/reset_password/:token", Account.AccountController, :reset_psw_by_token_post
    get "/verify_email/:token", Account.AccountController, :verify_email
  end
  # must login and verify email
  scope "/account", Walkmenu, as: :account do
    pipe_through [:browser, :attach_login_user_info, :must_login, :must_verify_login_user_email, :apply_layout]
    get "/", Account.AccountController, :profile
    put "/", Account.AccountController, :profile
    get "/change_password", Account.AccountController, :change_password
  end
  # must login no need verify email
  scope "/account", Walkmenu, as: :account do
    pipe_through [:browser, :attach_login_user_info, :must_login, :free_subscription, :apply_layout]

    get "/please_verify_your_email", Account.AccountController, :please_verify_your_email
    put "/please_verify_your_email", Account.AccountController, :please_verify_your_email
    get "/resend_verification_email", Account.AccountController, :resend_verification_email
  end

  ################################
  # public routes
  ################################
  scope "/", Walkmenu, as: :shared do
    pipe_through [:browser, :attach_login_user_info, :apply_layout]

    get "/page_not_found", Shared.ErrorController, :page_not_found
  end

  ################################
  # routes for diners
  ################################
  scope "/diner", Walkmenu, as: :diner do
    pipe_through [:browser, :attach_login_user_info, :must_login, :must_verify_login_user_email, :apply_layout]
    get "/orders", Diner.DinerController, :orders
  end
end
