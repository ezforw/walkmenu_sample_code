defmodule Walkmenu.RstDy do
  use Walkmenu.Web, :model

  @required_attrs [:enabled, :base_fee, :base_distance, :surcharge_per_km, :max_distance]

  @doc """
  Data structure for restaurant delivery settings
  """
  schema "rst_dy" do
    field :enabled, :boolean, default: false
    field :base_fee, :integer, default: 0
    field :base_distance, :integer, default: 0
    field :surcharge_per_km, :integer, default: 0
    field :max_distance, :integer, default: 30

    belongs_to :rst, Walkmenu.Rst
    belongs_to :user, Walkmenu.User

    timestamps()
  end

  @doc """
  Validate delivery max distance
  """
  defp check_max_distance(changeset) do
    base_distance = get_field(changeset, :base_distance)
    max_distance = get_field(changeset, :max_distance)
    if max_distance < base_distance do
      changeset |> add_error(:max_distance, "Max distance cannot be less than base distance")
    else
      changeset
    end
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_attrs)
    |> validate_required(@required_attrs])
    |> check_max_distance
  end
end
