defmodule Walkmenu.Services.Sms do
  use Walkmenu.Web, :controller

  @doc """
  Service for sending text messages to mobile phones
  """
  def send(message, number) do
    ExAws.SNS.publish(message, phone_number: number)
    |> ExAws.request([
        access_key_id: Application.get_env(:walkmenu, :sns_sms_key),
        secret_access_key: Application.get_env(:walkmenu, :sns_sms_secret)
      ])
  end
end
