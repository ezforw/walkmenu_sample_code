defmodule Walkmenu.Plug.MustVerifyLoginUserEmail do
  @behaviour Plug

  alias Walkmenu.SessionController, as: Session
  alias Walkmenu.Router.Helpers, as: Route

  import Plug.Conn, only: [halt: 1]
  import Phoenix.Controller, only: [put_flash: 3, redirect: 2]

  def init(opts), do: opts

  def call(conn, _opts) do
    # When this plug is plugged in, all requests should be blocked for users whos email is not verified
    if Session.login_user(conn).email_verified do
      conn
    else
      conn
        |> put_flash(:warn, "Please verify your email")
        |> redirect(to: Route.account_account_path(conn, :please_verify_your_email))
        |> halt
    end
  end
end
