defmodule Walkmenu.Plug.ApplyLayout do
  @behaviour Plug

  import Phoenix.Controller, only: [put_layout: 2]

  def init(opts), do: opts

  def call(conn, _opts) do
    # apply different layout based on login status
    if Walkmenu.SessionController.login_user(conn) do
      conn |> put_layout({Walkmenu.LayoutView, :layout_logged_in})
    else
      conn
    end
  end
end
