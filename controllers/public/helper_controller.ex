defmodule Walkmenu.Public.HelperController do
  use Walkmenu.Web, :controller

  @doc """
  Get restaurant info along with address and delivery settings
  """
  defp get_rst!(rst_id) do
    query = from r in Walkmenu.Rst,
      left_join: rst_addr in assoc(r, :rst_addr),
      left_join: rst_dy in assoc(r, :rst_dy),
      where: r.id == ^rst_id,
      preload: [rst_addr: rst_addr, rst_dy: rst_dy]
    Repo.one!(query)
  end

  @doc """
  Check if delivery information is valid or not
  """
  def invalidate_delivery_info(formatted_cq_params, rst) do
    if formatted_cq_params["order_type"] == "delivery" do
      if formatted_cq_params["delivery"] && Walkmenu.Cq.Delivery.required_changeset(%Walkmenu.Cq.Delivery{}, formatted_cq_params["delivery"]).valid? do
        case get_delivery_distance_and_fee(rst, formatted_cq_params["delivery"]["lat"], formatted_cq_params["delivery"]["lng"]) do
          {:ok, df} ->
            formatted_cq_params = formatted_cq_params
              |> put_in(["delivery", "fee"], df.fee)
              |> put_in(["delivery", "distance_from_rst"], df.distance)
            formatted_cq_params
          {:error, _} ->
            put_in(formatted_cq_params, ["delivery", "fee"], 0)
            |> put_in(["delivery", "formatted_address"], "")
        end
      else
        Map.put(formatted_cq_params, "delivery", %{"fee" => 0, "formatted_address" => ""})
      end
    else
      formatted_cq_params
    end
  end

  @doc """
  Calculate delivery distance and fee
  """
  def get_delivery_distance_and_fee(rst, lat, lng) do
    addr = rst.rst_addr

    di = HTTPoison.get! "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=#{addr.geometry.lat},#{addr.geometry.lng}&destinations=#{lat},#{lng}&mode=driving&units=metric&key=#{Application.get_env(:walkmenu, :google_map_distance_matrix_api_key)}"
    distance = Poison.decode!(di.body)
    distance = distance["rows"] |> List.first
    distance = distance["elements"] |> List.first
    distance = round(distance["distance"]["value"] / 1000)

    if distance > rst.rst_dy.max_distance do
      {:error, "Your delivery addreess is too far for us to deliver. Our max delivery distance is <b>#{rst.rst_dy.max_distance}km</b> away from our restaurant"}
    else
      price =
        if distance <= rst.rst_dy.base_distance do
          rst.rst_dy.base_fee
        else
          extra_km = distance - rst.rst_dy.base_distance
          rst.rst_dy.base_fee + round(extra_km * rst.rst_dy.surcharge_per_km)
        end
      {:ok, %{distance: distance, fee: price}}
    end
  end

  def calculate_delivery_fee(conn, %{"rst_id" => rst_id, "geo" => geo}) do
    rst = get_rst!(String.to_integer(rst_id))
    case get_delivery_distance_and_fee(rst, geo["lat"], geo["lng"]) do
      {:error, message} ->
        json conn, %{error: message}
      {:ok, df} ->
        json conn, %{distance: df.distance, delivery_fee: Walkmenu.Helpers.Currency.to_readable(df.fee)}
    end
  end

end
