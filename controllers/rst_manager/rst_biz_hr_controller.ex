defmodule Walkmenu.RstManager.RstBizHrController do
  use Walkmenu.Web, :controller
  alias Walkmenu.BizHr

  @doc """
  Homepage for businese hours
  """
  def index(conn, %{"rst_id" => rst_id}) do
    # get business hour from database
    user_id = Walkmenu.SessionController.managed_user(conn).id
    biz_hr = Repo.get_by!(BizHr, rst_id: String.to_integer(rst_id), user_id: user_id, type: "rst")

    render conn, "page_index.html",
      title: "Business Hours",
      rst_id: rst_id,
      changeset: BizHr.changeset(biz_hr),
      biz_hr: biz_hr
  end

  @doc """
  Update business hour in database
  """
  def update(conn, %{"biz_hr" => rbh, "rst_id" => rst_id}) do
    rst_id = String.to_integer(rst_id)
    user_id = Walkmenu.SessionController.managed_user(conn).id

    # get the record which belongs to the current user and resturant
    biz_hr =  Repo.get_by!(BizHr, [rst_id: rst_id, user_id: user_id, type: "rst"])

    # update the record
    changeset = BizHr.changeset(biz_hr, rbh)
    case Repo.update(changeset) do
      {:ok, _} ->
        conn
        |> put_flash(:success, "Update successful")
        |> redirect(to: rst_manager_rst_biz_hr_path(conn, :index, rst_id))
      {:error, changeset} ->
        conn = put_flash(conn, :error, "Failed to update")
        render conn, "page_index.html", title: "Business Hours", rst_id: rst_id, changeset: changeset, biz_hr: biz_hr
    end
  end

  @doc """
  Post request for updating delivery settings
  """
  def delivery(conn, %{"biz_hr" => params, "rst_id" => rst_id}) do
    rst_id = String.to_integer(rst_id)
    user_id = Walkmenu.SessionController.managed_user(conn).id

    # get delivery settings from database
    record = Repo.get_by!(BizHr, rst_id: rst_id, user_id: user_id, type: "rst_dy")

    # update database record
    changeset = BizHr.changeset(record, params)
    case Repo.update(changeset) do
      {:ok, _} ->
        conn
        |> put_flash(:success, "Update successful")
        |> redirect(to: rst_manager_rst_biz_hr_path(conn, :delivery, rst_id))
      {:error, changeset} ->
        conn = put_flash(conn, :error, "Failed to update")
        render conn, "page_delivery.html", title: "Delivery Hours", rst_id: rst_id, changeset: changeset, biz_hr: record
    end
  end

  @doc """
  Home page for delivery settings
  """
  def delivery(conn, %{"rst_id" => rst_id}) do
    user_id = Walkmenu.SessionController.managed_user(conn).id
    biz_hr = Repo.get_by!(BizHr, rst_id: String.to_integer(rst_id), user_id: user_id, type: "rst_dy")
    render conn, "page_delivery.html", title: "Delivery Hours", rst_id: rst_id, changeset: BizHr.changeset(biz_hr), biz_hr: biz_hr
  end

  @doc """
  Insert default delivery hours to database when a new restaurant is created
  """
  def insert_default_dy_hrs(rst_id, user_id) do
    changset = BizHr.changeset(%BizHr{}, %{
        type: "rst_dy",
        mon: %{},
        tue: %{},
        wed: %{},
        thu: %{},
        fri: %{},
        sat: %{},
        sun: %{}
      })
      |> Ecto.Changeset.put_change(:rst_id, rst_id)
      |> Ecto.Changeset.put_change(:user_id, user_id)
    Repo.insert!(changset)
  end

  @doc """
  Insert default business hours to database when a new restaurant is created
  """
  def insert_default_biz_hrs(rst_id, user_id) do
    changset = BizHr.changeset(%BizHr{}, %{
        type: "rst",
        mon: %{},
        tue: %{},
        wed: %{},
        thu: %{},
        fri: %{},
        sat: %{},
        sun: %{}
      })
      |> Ecto.Changeset.put_change(:rst_id, rst_id)
      |> Ecto.Changeset.put_change(:user_id, user_id)
    Repo.insert!(changset)
  end
end
