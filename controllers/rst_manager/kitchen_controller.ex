defmodule Walkmenu.RstManager.KitchenController do
  use Walkmenu.Web, :controller
  alias Walkmenu.CqMcItem

  @doc """
  Kitchen home page
  """
  def index(conn, %{"rst_id" => rst_id}) do
    # get today's client queue
    date = Timex.now(rst_timezone(conn)) |> Timex.to_date()
    cqs = get_all_cqs rst_id, date

    # validate websocket connection
    walkmenu_user = Walkmenu.SessionController.managed_user(conn)
    token = Phoenix.Token.sign(conn, "realtime-ordering", walkmenu_user.id)

    render conn, "page_index.html",
      title: "Kitchen",
      cqs: cqs,
      token: token,
      selected_date: Timex.to_date(Timex.now(rst_timezone(conn))),
      rst_id: rst_id
  end

  def dateline(conn, %{"rst_id" => rst_id, "date" => date}) do
    date = Walkmenu.RstManager.CqController.get_date_from_string date
    cqs = get_all_cqs rst_id, date

    walkmenu_user = Walkmenu.SessionController.managed_user(conn)
    token = Phoenix.Token.sign(conn, "realtime-ordering", walkmenu_user.id)

    render conn, "page_index.html",
      title: "Kitchen",
      cqs: cqs,
      rst_id: rst_id,
      token: token,
      selected_date: date
  end

  @doc """
  Start cooking a dish
  """
  def start_cooking(conn, %{"cq_mc_item_id" => dish_id, "rst_id" => rst_id}) do
    change_dish_status conn, String.to_integer(dish_id), "cooking", rst_id
  end

  @doc """
  Dish is being cooked
  """
  def finish_cooking(conn, %{"cq_mc_item_id" => dish_id, "rst_id" => rst_id}) do
    change_dish_status conn, String.to_integer(dish_id), "ready", rst_id
  end

  @doc """
  Retrieve missing client queue information when client's internet connection is reconnected
  """
  def missing_queues(conn, %{"date" => date, "rst_id" => rst_id, "last_cq_id" => last_cq_id}) do
    date = Walkmenu.RstManager.CqController.get_date_from_string date
    query = from cq in Walkmenu.Cq,
      left_join: dishes in assoc(cq, :cq_mc_items),
      left_join: addons in assoc(dishes, :cq_mci_addons),
      left_join: options in assoc(addons, :cq_mcia_options),
      where: cq.id > ^last_cq_id and cq.rst_id == ^rst_id and cq.order_date == ^date and (cq.status == "in_queue" or cq.status == "w4_arrival"),
      order_by: [asc: cq.order_time, asc: dishes.id, asc: addons.id, asc: options.id],
      preload: [cq_mc_items: {dishes, cq_mci_addons: {addons, cq_mcia_options: options}}],
      limit: 50
    cqs =
      Repo.all(query)
      |> Enum.map(fn cq ->
        html = Phoenix.View.render_to_string(Walkmenu.RstManager.KitchenView, "partial_page_single_cq.html", cq: cq, conn: conn, rst_id: rst_id)
        %{html: html}
      end)
    json conn, %{cqs: cqs}
  end

  @doc """
  Change dish status and notify web clients
  """
  defp change_dish_status(conn, dish_id, status, rst_id) do
    case Repo.get(CqMcItem, dish_id) do
      nil ->
        json conn, %{success: false, error: "Dish not found"}
      dish ->
        update = from(i in CqMcItem,
                  update: [set: [status: ^status]],
                  where: i.id == ^dish_id)
               |> Repo.update_all([])
        case update do
          {0, _} ->
            json conn, %{success: false, error: "Dish not updated"}
          _ ->
            cq = get_single_cq(String.to_integer(rst_id), dish.cq_id)
            Walkmenu.RstManager.CqController.broadcast_cq_changes_to_counter conn, cq, rst_id
            Walkmenu.PublicCqChannel.broadcast_cq_updated(%{
              html: Phoenix.View.render_to_string(
                Walkmenu.Public.CqView, "partial_cq.html", cq: cq
              )
            }, Integer.to_string(cq.id));
            broadcast_cq_changes_to_kitchen conn, cq, rst_id
            json conn, %{success: true}
        end
    end
  end

  @doc """
  Automatically notify client queue changes to kitchen
  """
  def broadcast_cq_changes_to_kitchen(conn, cq, rst_id) do
    html = Phoenix.View.render_to_string(Walkmenu.RstManager.KitchenView, "partial_page_single_cq.html", cq: cq, conn: conn, rst_id: rst_id)
    Walkmenu.RstKitchenChannel.broadcast_cq_changed(%{new_cq: html, cq_id: cq.id}, rst_id)
  end

  @doc """
  Retrieve a particular client queue information
  """
  def get_single_cq(rst_id, cq_id) do
    query = from cq in Walkmenu.Cq,
                left_join: dishes in assoc(cq, :cq_mc_items),
                left_join: addons in assoc(dishes, :cq_mci_addons),
                left_join: options in assoc(addons, :cq_mcia_options),
                where: cq.rst_id == ^rst_id,
                order_by: [asc: dishes.id, asc: addons.id, asc: options.id],
                preload: [cq_mc_items: {dishes, cq_mci_addons: {addons, cq_mcia_options: options}}]
    Repo.get(query, cq_id)
  end

  @doc """
  Retrieve the timezone of the current restaurant
  """
  defp rst_timezone(conn) do
    conn.assigns[:rst].timezone
  end

  @doc """
  Retrieve all client queues from a particular restaurant within a particular date
  """
  def get_all_cqs(rst_id, date) do
    query = from cq in Walkmenu.Cq,
                left_join: dishes in assoc(cq, :cq_mc_items),
                left_join: addons in assoc(dishes, :cq_mci_addons),
                left_join: options in assoc(addons, :cq_mcia_options),
                where: cq.rst_id == ^rst_id and cq.order_date == ^date and cq.status == "in_queue" and dishes.status != "served" and dishes.status != "ready",
                order_by: [asc: cq.order_time, asc: dishes.id, asc: addons.id, asc: options.id],
                preload: [cq_mc_items: {dishes, cq_mci_addons: {addons, cq_mcia_options: options}}],
                limit: 100
    Repo.all(query)
  end
end
