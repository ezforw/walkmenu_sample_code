defmodule Walkmenu.RstKitchenChannel do
  use Walkmenu.Web, :channel

  @doc """
  Get realtime kitchen data updates
  """
  def join("rst:kitchen:" <> rst_id, params, socket) do
    # When the internet is interrupted and reconnected, push the latest data
    # to front end
    if Map.has_key?(params, "initial_connection") && params["initial_connection"] != true do
    if Map.has_key?(params, "last_cq_id") && params["last_cq_id"] != 0 do
      if Map.has_key?(params, "date") do
        send(self(), {:after_join, params |> Map.put("rst_id", rst_id)})
      end
    end
    end
    {:ok, "joined " <> rst_id, socket}
  end

  @doc """
  Fetch client queue data
  """
  def handle_info({:after_join, params}, socket) do
    rst = Repo.one(from(r in Walkmenu.Rst, select: %{timezone: r.timezone}, where: r.id == ^params["rst_id"]))
    case rst do
      nil ->
        {:noreply, socket}
      _rst ->
        date = Walkmenu.RstManager.CqController.get_date_from_string(params["date"])
        cqs =
          Repo.one(from(
            cq in Walkmenu.Cq,
            select: %{count: count(cq.id)},
            where: cq.rst_id == ^params["rst_id"] and cq.id > ^params["last_cq_id"] and cq.order_date == ^date and (cq.status == "in_queue" or cq.status == "w4_arrival")
          ))
        push socket, "missing_cqs", %{cqs: cqs}
        {:noreply, socket}
    end
  end

  @doc """
  Broadcast to all devices when a client is added to the queue
  """
  def broadcast_cq_added(payload, rst_id) do
    Walkmenu.Endpoint.broadcast("rst:kitchen:" <> rst_id, "kitchen_cq_added", payload)
  end

  @doc """
  Broadcast to all devices when a client is removed from the queue
  """
  def broadcast_cq_left(payload, rst_id) do
    Walkmenu.Endpoint.broadcast("rst:kitchen:" <> rst_id, "kitchen_cq_left", payload)
  end

  @doc """
  Broadcast to all devices when a client's status has changed
  """
  def broadcast_cq_changed(payload, rst_id) do
    Walkmenu.Endpoint.broadcast("rst:kitchen:" <> rst_id, "kitchen_cq_changed", payload)
  end

end
