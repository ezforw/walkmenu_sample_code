defmodule Walkmenu.RstSocket do
  use Phoenix.Socket

  ## Channels
  channel "rst:kitchen:*", Walkmenu.RstKitchenChannel

  ## Transports
  transport :websocket, Phoenix.Transports.WebSocket,
    timeout: 45_000
  # transport :longpoll, Phoenix.Transports.LongPoll

  # Socket params are passed from the client and can
  # be used to verify and authenticate a user.
  #
  #     {:ok, assign(socket, :user_id, verified_user_id)}
  #
  # To deny connection, return `:error`.
  def connect(%{"token" => token}, socket) do
    case Phoenix.Token.verify(socket, "realtime-ordering", token, max_age: 1209600) do
      {:ok, _user_id} ->
        {:ok, socket}
      {:error, _reason} ->
        :error
    end
  end

  # Returning `nil` makes this socket anonymous.
  def id(_socket), do: nil
end
