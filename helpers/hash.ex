defmodule Walkmenu.Helpers.Hash do

  @doc """
  Generates a uuid string
  """
  def uuid do
    Walkmenu.Helpers.Uuid.uuid4
      |> Walkmenu.Helpers.Uuid.uuid5(hash_key(), :hex)
  end

  @doc """
  Hash a token string
  """
  def make_token(token) do
    sha256(hash_key(), token)
  end

  @doc """
  Encryption using sha256
  """
  defp sha256(key, string) do
    :crypto.hmac(:sha256, key, string) |> Base.encode16
  end

end
