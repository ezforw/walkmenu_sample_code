import {Socket} from "phoenix"
import networkListener from './../helpers/online_offline_listener';

// Setup websocket for realtime table management
export var TableSocket = {
  run() {

    // detect internet status(online/offline)
    networkListener.run();
    let socket = new Socket("/socket", {
      params: {
        token: CAHNNEL_TOKEN
      }
    })

    socket.connect()

    let table = socket.channel("rst:table:" + wmjs.rst_id, {})

    table.on("table_status_changed", payload => {
      $(document).find('#table-' + payload.table_id).html($(payload.table_html).html());
    })

    table.join().receive("ok", resp => {
      // console.log("Joined successfully", resp)
    }).receive("error", resp => {
      console.log("Unable to join", resp)
    })
  }
}
