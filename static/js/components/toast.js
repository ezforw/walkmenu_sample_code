import icons from './../icons'
export default {
  // show the toast message
  _show(message, type, icon) {
    var toast = $(`<div style="display:none;" class="toast ${type}"><div class="content">${icon}<div class="message">${message}</div><div class="close icon">${icons.svgClose()}</div></div></div>`);
    if ($(window).width() < wmjs.screenSize.sm - 1) {
      $(document).find('.toast-stack').append(toast);
    } else {
      $(document).find('.toast-stack').prepend(toast);
    }

    toast.velocity("transition.slideDownIn", {duration: 300});
    return toast;
  },
  // show the toast message with delay
  _show_with_delay(message, type, icon, config) {
    config = config || {};
    var show = this._show;
    var removeToast = this._remove_toast;
    if ("autoHide" in config) {} else {
      config.autoHide = true;
    }

    config.duration = config.duration || 8000;

    setTimeout(function() {
      var toast = show(message, type, icon);
      if (config.autoHide) {
        setTimeout(function() {
          removeToast(toast);
        }, config.duration);
      }
    }, 150);
  },
  // remove a given toast object
  _remove_toast(toast) {
    toast.velocity("transition.slideUpOut", {
      duration: 300,
      complete: function() {
        toast.remove();
      }
    })
  },
  // remove all toast
  clearAll() {
    $(document).find('.toast-stack .toast').each(function() {
      wmjs.toast._remove_toast($(this));
    });
  },
  // success toast
  success(message = '', config = {}) {
    var type = 'success';
    var icon = icons.svgCheck('type-icon ' + type);
    this._show_with_delay(message, type, icon, config);
  },
  // error toast
  error(message = '', config = {}) {
    var type = 'error';
    var icon = icons.svgAlert('type-icon ' + type);
    if ("autoHide" in config) {} else {
      config.autoHide = false;
    }
    this._show_with_delay(message, type, icon, config);
  },
  // info toast
  info(message = '', config = {}) {
    var type = 'info';
    var icon = icons.svgInfo('type-icon ' + type);
    this._show_with_delay(message, type, icon, config);
  },
  // warning toast
  warn(message = '', config = {}) {
    var type = 'warn';
    var icon = icons.svgAlert('type-icon ' + type);
    this._show_with_delay(message, type, icon, config);
  }
}
