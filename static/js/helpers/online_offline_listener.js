export default {
  run() {
    // Show warning when internet is offline
    window.addEventListener('offline', function() {
      if (wmjs.refreshPageTimer) {
        wmjs.toast.clearAll();
        clearTimeout(wmjs.refreshPageTimer);
      }
      wmjs.toast.error('Your internet is offline');
    });
  }
}
